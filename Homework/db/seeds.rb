# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

insurance_list = [
    [ "Aberdeen Insurance Co", "10370 Richmond Ave, Houston, TX 77042" ],
    [ "ACE American Insurance Co", "535 West Broadway, Dallas, TX 31782" ],
    [ "Aetna Insurance Co", "151 Farmington Ave, Hartford, CT 05434" ],
    [ "AIU Insurance Co", "21650 Oxnard St., Woodland Hills, CA 91547" ],
    [ "Cigna Insurance Co", "1851 East First St, Santa Ana, CA 91823" ]
]

insurance_list.each do |name, street_address|
  Insurance.create( name: name, street_address: street_address )
end