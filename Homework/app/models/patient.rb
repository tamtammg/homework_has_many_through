class Patient < ActiveRecord::Base
  has_many :appointments
  belongs_to :insurance
  has_many :specialists, through: :appointments
end
